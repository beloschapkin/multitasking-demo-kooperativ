/*********************************************************************\
* Dateiname: mtd_api.h
* Autor    : Dr. Lurz
* Projekt  : Multitasking Demo Kooperativ
*
* Copyright (C) Dr. Lurz
*
* Kurzbeschreibung: 
* Enthaelt Konfigurationsdaten zur Demo von kooperativem Multitasking
*
* Datum:     Autor:       Grund der Aenderung:
* 30.09.08   Dr. Lurz     Neuerstellung
*
\*********************************************************************/
#ifndef MTD_API_H
#define MTD_API_H

/*--- #includes der Form <...> --------------------------------------*/

/*--- #includes der Form "..." --------------------------------------*/

/*--- #define-Konstanten und Makros ---------------------------------*/

#define TNUM  3
#define TQMAX 10


#define T_TSKSCHED 'U'

#define T_TSKACTIV 'Y'


#define T_INTTM    'I'
#define T_INT2     'J'

#define T_BGNT0    'A'
#define T_RUNT0    'a'
#define T_ENDT0    'b'

#define T_BGNT1    'C'
#define T_RUNT1    'c'
#define T_ENDT1    'd'

#define T_BGNT2    'E'
#define T_RUNT2    'e'
#define T_ENDT2    'f'

#define T_BGNT3    'G'
#define T_RUNT3    'g'
#define T_ENDT3    'h'

/*--- Datentypen (typedef) ------------------------------------------*/

/*--- Globale Konstanten --------------------------------------------*/

/*--- Globale Variablen ---------------------------------------------*/

/*--- Prototypen globaler Funktionen --------------------------------*/
void actv_t( unsigned int tn );
void sched_nxt_t( void );

void trc_log( char tv, int tID );
void trc_init(void);

void io_puts(char *pc);
int  io_getc( void );

int  t_run( void );
void sys_init( void );
void sys_stop( void );


void task_0( void );
void task_1( void );
void task_2( void );
//void task_3( void );

void ti_func( void );


#endif /* MTD_API_H */
