/*********************************************************************\
* Dateiname: mtd_sys.C
* Autor    : Dr. Lurz
* Projekt  : Multitasking Demo Kooperativ
*
* Copyright (C) Dr. Lurz
*
* Kurzbeschreibung: 
* Enthaelt Systemfunktionen zur Demo von kooperativem Multitasking
* (f�r verschiedene Compiler / Umgebungen)
*
* Datum:     Autor:       Grund der Aenderung:
* 30.09.08   Dr. Lurz     Neuerstellung
*                         - Windows VC++:       cl    n1.c n2.c 
*                         - Windows BCC32:      bcc32 n1.c n2.c
*                         - Win/DOS BCC 16-Bit: bcc   n1.c n2.c  
*                         - Linux / pThreads:   gcc -pthread n1.c n2.c 
*
\*********************************************************************/
//#ifndef XXX__        /* Test only */
#ifdef _MSC_VER      /* Wird von MS-VC++ gesetzt */ 
#define WIN__
#elif __BORLANDC__   /* Wird von Borland-C Compilern gesetzt */
#define WIN__
#elif __MINGW32__    /* Wird von GNU-C Compilern auf Windows gesetzt */
#define WIN__
#elif __GNUC__       /* Wird von GNU-C Compilern gesetzt */
#define POSIX__
#endif

#ifdef __MSDOS__     /* Wird von Borland-C 16 Bit Compilern gesetzt */
#define B16__  
#endif
//#endif

#define _CRT_SECURE_NO_WARNINGS


/*--- #includes der Form <...> --------------------------------------*/
#include <stdio.h>
//#include <stdlib.h>

#ifdef WIN__
#include <conio.h>
#endif

#ifdef B16__
#include <dos.h>
#endif

/*--- #includes der Form "..." --------------------------------------*/
#include "mtd_api.h"

/*--- #define-Konstanten und Makros ---------------------------------*/

//#define FFS  1   /* FIF0 Scheduling */
#define RRS  3   /* Round-Robin Scheduling */


//#define putch _putch
//#define kbhit _kbhit
//#define getch _getch


/*--- Datentypen (typedef) ------------------------------------------*/

/*--- Globale Konstanten --------------------------------------------*/

/*--- Globale Variablen ---------------------------------------------*/

/*--- Modullokale Konstanten ----------------------------------------*/

/*--- Modullokale Variablen -----------------------------------------*/

/* pts[]: Function-Pointers to Tasks 0, 1, 2, "End" */
static void (* const pts[])(void) = { task_0, task_1, task_2, 0, 0 };

/* tq: Task-Queue; itq: IndexToQueue */
static int tq[TQMAX+1];
static int itq = 0;

static FILE *pfw;


/* tcb[]: Task States for Task 0, 1, 2; used f�r RR-Scheduling */
#define S_IDLE  0
#define S_READY 1

static int tcb[TNUM] = { S_READY, S_IDLE, S_IDLE };

   
/*--- Prototypen modullokaler Funktionen ----------------------------*/


/*--- Funktionsdefinitionen -----------------------------------------*/

/*********************************************************************\
* Funktionsname: actv_t (Activate-Task) 
\*********************************************************************/
void actv_t( unsigned int tn )
{
   static int iqw;
   static int svar;

   if ( tn >= TNUM )
      return;
   
# ifndef B16__  
   while ( svar != 0 );   //Sync-Var; Mutual Exclusion
   svar = 1;
# else
   disable();
# endif

   trc_log(T_TSKACTIV, tn);
   
   // Store Task in Queue: tn
   /* Hier Code erg�nzen */
   /***/

# ifdef FFS
   if (itq >= TQMAX - 1){
      // do nothing
   } else {
      tq[itq] = tn;
      itq++;
   }
# endif

# ifdef RRS
   tcb[tn] = S_READY;
# endif

   /***/
   svar = 0;
# ifdef B16__
   enable();
# endif
}

/*********************************************************************\
* Funktionsname: sched_nxt_t (Scheduling) 
\*********************************************************************/
void sched_nxt_t( void )
{
   static int iqr;
   unsigned int tn=0;
   
   // Search next Task: tn
   /* Hier Code erg�nzen */
   /***/

# ifdef FFS
   if (itq <= 0) {
      // do nothing
   } else {
      tn = tq[0];
      for (int i = 0; i < TQMAX - 1; i++){
         tq[i] = tq[i+1];
      }
      itq--;
   }
# endif

# ifdef RRS
   if (tcb[itq] != 0) {
      if (tcb[itq] == S_READY){
         tn = itq;
         tcb[itq] = S_IDLE;
      }
   }
   
   if (itq >= TNUM - 1){
      itq = 0;
   } else {
      itq++;
   }
# endif


   /***/
   trc_log(T_TSKSCHED, tn);
   
   // Run Task: tn
   (*pts[tn])();
   
}


/*********************************************************************\
* Funktionsname: Trace-Funktionen
\*********************************************************************/
void trc_log( char tv, int tID )
{
   static int svar;
   
# ifndef B16__     
   while ( svar != 0 );   //Sync-Var; Mutual Exclusion
   svar = 1;
# else
   disable();
# endif

#  ifdef __BORLANDC__ 
   switch ( tv )
   {
         case 'A': case 'a': case 'b':
           textcolor( LIGHTGREEN );
           break;
         case 'C': case 'c': case 'd':
           textcolor( YELLOW );
           break;
         case 'E': case 'e': case 'f':
           textcolor( LIGHTMAGENTA );
           break;
         case 'I':
            textcolor( LIGHTRED );
           break;
         case 'K': case 'k':
           textcolor( LIGHTRED );
           break;
         default:
           textcolor( LIGHTGRAY ); //WHITE );
           break;
   }
#  endif

#  ifdef WIN__
   putch(tv);
#  else
   putchar(tv);
#  endif
   if ( pfw ) fputc( tv, pfw );

   if ( tv >= 'J' && tv <= 'Z' ) // || tv >= 'i' && tv <= 'z' )
   {
#     ifdef WIN__
      putch(tID + '0');
#     else
      putchar(tID + '0');
#     endif
      if ( pfw ) fputc( tID+'0', pfw );
      //if ( tID == 0)
      putchar(' ');
      if ( pfw ) fputc( ' ', pfw );
   } 
   else if ( tv == 'I' || tv == 'b' || tv == 'd' || tv == 'f' )
   {
      putchar(' ');
      if ( pfw ) fputc( ' ', pfw );
   }
   fflush(stdout);
   
   svar = 0;
# ifdef B16__
   enable();
# endif
}


/*********************************************************************/
void trc_init( void )
{
   pfw = fopen("Trace.txt", "w");
}


/*********************************************************************\
* Funktionsname: IO-Funktionen
\*********************************************************************/
void io_puts( char *pc )
{
   puts( pc );
   fputs( pc, pfw );
}


/*********************************************************************/
int io_getc( void )
{
   //int c;
#  ifdef WIN__   
   if ( kbhit() )
   {
      //c = getch();
      return getch();
   }
   else
#  endif   
      return 0; 
}



/*********************************************************************\
* Funktionsname: t_run / schedule 
\*********************************************************************/
#ifndef B16__
#ifdef WIN__ 
#include <windows.h>
#else
#include <pthread.h>
#include <unistd.h>
#endif
#else
//#include <dos.h>
#endif

static volatile int run=1;

int t_run( void )
{
   unsigned int nn=0;

   //actv_t(0);
   while ( run == 1 && ++nn < 200 ) 
   { 
       sched_nxt_t();
   }
   
   run = 0;
   
   /* Stop / Ende; ggf. noch kurz warten auf Thread */
# ifndef B16__
#  ifdef WIN__ 
   Sleep(1010);
#  else
   usleep(1010*1000);
#  endif
# endif

   return nn;
}


/*********************************************************************\
* Funktionsname: ThreadFunc / Timer-Int-Func  
\*********************************************************************/ 
#ifndef B16__
static void *t_func( void *pA )
{
   while ( run == 1 )
   {
#     ifdef WIN__ 
      Sleep(1000);   //Aufwachen nach xx msec
#     else
      usleep(1000*1000);   //Aufwachen nach xx msec
#     endif
      
      ti_func();
   }
   return 0;
}
#else
/* BORLAND_16_Bit_ */
static void interrupt (*old_tick_isr)(void);

static void interrupt ti_tick_isr( void )
{
      static unsigned int nin;
      /*
      asm {
         int 0f2H              //;run old_tick_isr
      }
      */
      old_tick_isr();
      
      if ( (nin++ % 20) == 0 )
         ti_func();
}
#endif



/*********************************************************************\
* Funktionsname: Sys-Init-Funktionen
\*********************************************************************/  
void sys_init( void )
{
# ifndef B16__
#  ifdef WIN__ 
   DWORD tId;
#  else
   pthread_t tHandle;
   //int tNum=0;
#  endif
# endif

#  ifdef __BORLANDC__
   clrscr();
#  endif


#  ifdef FFS
   io_puts( "MT-FIFO Scheduling " );
#  elif  RRS
   io_puts( "MT-RR Scheduling " );
#  else
   io_puts( "MT-... Scheduling " );
#  endif


# ifndef B16__
#  ifdef WIN__    
   CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)t_func, NULL, 0, NULL );
   //_beginthreadex( NULL, 0, t_func, NULL, 0, &tId );
#  else
   pthread_create( &tHandle, NULL, t_func, (void *)NULL );
#  endif
# else
   /* BORLAND_16_Bit_ */
   /* Timer-ISR installieren, vorige Timer-ISR sichern / umsetzen */
   old_tick_isr = getvect( 0x08 );
   //setvect( 0xF2, old_tick_isr );
   setvect( 0x08, (void interrupt (*)(void)) ti_tick_isr );
   io_puts( "BCC-16 " );   
# endif
}

/*********************************************************************/
void sys_stop( void )
{
   run = 0;
   
# ifdef B16__   
   /* BORLAND_16_Bit_ */   
   setvect( 0x08, old_tick_isr );
# endif
}
