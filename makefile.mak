#Beispiel-Makefile zur Generierung mit Borland-Compiler BCC 
#(Paths f�r BCC und Applikations-Verzeichnisse sind ggf. anzupassen)
#Verwendung: make -fmakefile.mak
#                                               2007, Bruno Lurz
#---------------------------------------------------------------
#REM; SET PATH=%PATH%;C:\BC5\Bin

BC_H = V:\Fak_EFI\Labor_Embedded_Systems\APS\BCC55\\
#BC_H = C:\BC5\\
#BC_BIN = C:\BC5\Bin\\
#BC_LIB = C:\BC5\Lib\\
#BC_INC = C:\BC5\Include\\


mtdsys_X.exe: mtdsys_X.c mtdappl.c mtd_api.h
	$(BC_H)Bin\BCC32 -I$(BC_H)Include mtdsys_X.c mtdappl.c


