/*********************************************************************\
* Dateiname: mtd_appl.C
* Autor    : Dr. Lurz
* Projekt  : Multitasking Demo Kooperativ
*
* Copyright (C) Dr. Lurz
*
* Kurzbeschreibung: 
* Enthaelt Demo Applikation Tasks zur Demo von kooperativem Multitasking
*
* Datum:     Autor:       Grund der Aenderung:
* 30.09.08   Dr. Lurz     Neuerstellung
*
\*********************************************************************/

/*--- #includes der Form <...> --------------------------------------*/
#include <stdio.h>
//#include <stdlib.h>

/*--- #includes der Form "..." --------------------------------------*/
#include "mtd_api.h"

/*--- #define-Konstanten und Makros ---------------------------------*/

/*- Defines "Length of Tasks" -*/
#define LOOP_CNT_0   4
#define LOOP_CNT_1   4
#define LOOP_CNT_2   4


#define W_CNT   40000000UL

#define DO_WORK(n) \
        { \
           volatile unsigned long int i; \
           for ( i=0; i < n; i++ ); \
        }

/*--- Datentypen (typedef) ------------------------------------------*/

/*--- Globale Konstanten --------------------------------------------*/

/*--- Globale Variablen ---------------------------------------------*/

/*--- Modullokale Konstanten ----------------------------------------*/

/*--- Modullokale Variablen -----------------------------------------*/

/*--- Prototypen modullokaler Funktionen ----------------------------*/

/*********************************************************************\
* Funktionsname: inp
* Behandlung von Benutzer/User-Eingaben 
\*********************************************************************/
static void ui_inp( void )
{   
   int in;

   if ( (in = io_getc()) != 0 )
   {  
      if ( in < '0' || in > '7' )
      {
         sys_stop();
         putchar( 'X' );
      }
      else
      {
         in &= 0x07;               /* 0 <= in <= 7 */
         
         if ( in < TNUM )          /* Activate Task "in" */
         {
            actv_t(in);
         }
         else if ( in < TNUM+2 )   /* Activate Task "1"&"2" */
         {
            actv_t(1); 
            actv_t(2);
         }
         else    /* in >= TNUM+2 *//* Activate Task "2"&"1" */
         {
            actv_t(2); 
            actv_t(1);
         }
      }
   }
}


/*********************************************************************\
* Funktionsname: task_0 (Applikation, IDLE-Task) 
\*********************************************************************/
void task_0( void )
{
   static int cnts;
   int  cnt;   //= 0;

   trc_log( T_BGNT0, 0 );
   
   ui_inp();
   
   //for (cnt = 0; cnt < LOOP_CNT_0; cnt++)
   {
      DO_WORK( W_CNT );
      
      //trc_log( T_RUNT0, 0 );
   }
   
   cnts++;   
   trc_log( T_ENDT0, 0 );
}

/*********************************************************************\
* Funktionsname: task_x (Applikation) 
\*********************************************************************/
void task_1( void )
{
   static int cnts;
   int cnt;
   
   trc_log( T_BGNT1, 1 );

   for (cnt = 0; cnt < LOOP_CNT_1; cnt++)
   {
      DO_WORK( W_CNT );
     
      trc_log( T_RUNT1, 1 );
   }
   
   //if ( cnts % 3 == 0 ) 
     // actv_t(0);
 
   cnts++;   
   trc_log( T_ENDT1, 1 );
}

/*********************************************************************\
* Funktionsname: task_x (Applikation) 
\*********************************************************************/
void task_2( void )
{
   static int cnts;
   int cnt;
   
   trc_log( T_BGNT2, 2 );

   for (cnt = 0; cnt < LOOP_CNT_2; cnt++)
   {
      DO_WORK( W_CNT );
      
      trc_log( T_RUNT2, 2 ); 
   }
   
   if ( cnts % 2 == 0 ) 
      actv_t(1);
   
   cnts++;   
   trc_log( T_ENDT2, 2 );
}

/*********************************************************************\
* Funktionsname: TimerFunc (Int) 
\*********************************************************************/ 
void ti_func( void )
{
   static int cnts;

   trc_log( T_INTTM, 8 );

   if ( cnts % 5 == 0) 
      actv_t(1);
   
   if ( cnts % 3 == 0) 
      actv_t(2);
   
   if ( cnts % 6 == 0) 
      actv_t(1);
   
   cnts++;
}



/*********************************************************************/


/*********************************************************************\
* Funktionsname: main 
\*********************************************************************/
int main( void )
{
   int nn;
   
   trc_init();   
   sys_init();
   
   io_puts( "MT-Schedule Start \n" );
   io_puts("#> ");

   nn = t_run();
   
   io_puts(" <# ");
   
   printf("\nEnde; Schedules: %d \n", nn);
   getchar();

   return 0;
}

